// soal no 1
const luas = () => {
    panjang = 5
    lebar = 4
    Luas = panjang * lebar
    return Luas
}
const keliling = () => {
    panjang = 5
    lebar = 4
    Keliling = 2 * (panjang + lebar)
    return Keliling
}
console.log(luas())
console.log(keliling())

// soal no 2
const newFunction = (firstname, lastname) => {
    return {
        first: firstname,
        last: lastname,
        full: function () {
            console.log(this.first + " " + this.last)
        }
    }
}
newFunction("william", "imoh").full()

// soal no 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const { firstName, lastName, address, hobby } = newObject
console.log(firstName, lastName, address, hobby)

// soal no 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combinearray = [...west, ...east]
console.log(combinearray)

// soal no 5
const planet = "earth"
const view = "glass"
var before = `${planet} ${view}`
console.log(before)
