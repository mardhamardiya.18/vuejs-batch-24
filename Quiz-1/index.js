// soal no 1
function next_date(tanggal, bulan, tahun) {
    var tgl = tanggal + 1
    var bln = " "
    var thn = tahun
    switch (bulan) {
        case 1:
            bln = " Januari "
            break;
        case 2:
            bln = " Februari "
            break;
        case 3:
            bln = " Maret "
            break;
        case 4:
            bln = " April "
            break;
        case 5:
            bln = " Mei "
            break;
        case 6:
            bln = " Juni "
            break;

        default:
            bln = " Juli "
            break;
    }
    return tgl + bln + thn
}
var tanggal = 29
var bulan = 2
var tahun = 2020

console.log(next_date(tanggal, bulan, tahun)) // output : 1 Maret 2020


// soal no 2
function jumlah_kata(string) {
    isi = string.split(" ")
    return isi.length
}

var kalimat_1 = "Hallo nama saya Mardha Mardiya"
var kalimat_2 = "Saya mardha"

console.log(jumlah_kata(kalimat_1))
console.log(jumlah_kata(kalimat_2))