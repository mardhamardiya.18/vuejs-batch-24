// soal no 1
var nilai = 70

if (nilai >= 85) {
    console.log('indek = A')
} else if (nilai >= 75 && nilai < 85) {
    console.log('index = B')
} else if (nilai >= 65 && nilai < 75) {
    console.log("index = C")
} else if (nilai >= 55 && nilai < 65) {
    console.log("index = D")
} else {
    console.log("index = E")
}

// soal no 2
var tanggal = 18
var bulan = 2
var tahun = 2001

switch (bulan) {
    case 1:
        console.log("18 Januari 2001")
        break
    case 2:
        console.log("18 Februari 2001")
        break
    case 3:
        console.log("18 Maret 2001")
    default:
        console.log("tidak ada bulan")
}

// soal no 3
function segitiga1(panjang) {
    let hasil = '';
    for (let i = 0; i < panjang; i++) {
        for (let j = 0; j <= i; j++) {
            hasil += '# ';
        }
        hasil += '\n';
    }
    return hasil;
}
console.log(segitiga1(3));

// soal no 4

var m = 7
for (var i = 1; i <= m; i++) {

    j = i % 3

    switch (j) {
        case 1:
            console.log(i + "- I Love Programming")
            break;
        case 2:
            console.log(i + "- I Love Javascript")
            break;

        default:
            console.log(i + "- I Love Vuejs")

            var simbol = ""
            for (var k = 0; k < i; k++) {
                simbol += '='

            }
            console.log(simbol)
            break;
    }
}

