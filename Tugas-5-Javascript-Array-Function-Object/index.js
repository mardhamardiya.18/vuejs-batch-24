// soal no 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();
var arraylength = daftarHewan.length;
for (var i = 0; i < arraylength; i++) {
    console.log(daftarHewan[i]);
}

// soal no 2
function introduce(data) {
    return "Nama Saya " + data.name + ", umur saya " + data.age + " tahun, alamat saya di " + data.address + ", dan saya punya hobby yaitu " + data.hobby
}

var data = { name: "John", age: 30, address: "Jalan Pelesiran", hobby: "Gaming" }

var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 

// soal no 3

function hitung_huruf_vokal(nilai) {
    var teksArray = nilai.split("");
    var jumlahVokal = 0;
    for (var n = 0; n < teksArray.length; n++) {
        if (cekvokal(teksArray[n]) === true) jumlahVokal++;
    }
    return jumlahVokal;
}
function cekvokal(c) {
    return ['a', 'e', 'i', 'o', 'u'].indexOf(c.toLowerCase()) !== -1;
}
var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1, hitung_2) // 3 2

// soal no 4
function hitung(angka) {

    return (angka * 2) - 2;
}
console.log(hitung(0)) // -2
console.log(hitung(1)) // 0
console.log(hitung(2)) // 2
console.log(hitung(3)) // 4
console.log(hitung(5)) // 8